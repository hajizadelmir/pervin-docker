package az.ingress.ms9.controller;

import az.ingress.ms9.dto.StudentDto;
import az.ingress.ms9.service.StudentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

        private final StudentServiceImpl service;

        @GetMapping("/{id}")
        public StudentDto getStudentById(@PathVariable Long id){
            return service.getStudentById(id);
        }
        @GetMapping
        public List<StudentDto> getAll(){
            return service.findAllStudent();
        }
        @PostMapping
        public StudentDto createStudent(@RequestBody StudentDto dto){
            return service.createStudent(dto);
        }
        @DeleteMapping("/{id}")
        public void deleteStudent(@PathVariable Long id){
            service.deleteStudent(id);
        }
        @PutMapping
        public StudentDto updateStudent (@RequestBody StudentDto dto){
            return service.updateStudent(dto);
        }

}
