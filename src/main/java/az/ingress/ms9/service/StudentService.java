package az.ingress.ms9.service;

import az.ingress.ms9.dto.StudentDto;

import java.util.List;

public interface StudentService {
    public StudentDto getStudentById(Long id);

    public StudentDto createStudent(StudentDto dto);

    public void deleteStudent(Long id);

    public StudentDto updateStudent(StudentDto dto);

    List<StudentDto> findAllStudent();

}
